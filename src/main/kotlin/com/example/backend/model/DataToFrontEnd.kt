package com.example.backend.model

data class DataToFrontEnd(
    val covidFalleAktuell: Int,
    val bettenBelegt: Int,
    val bettenFrei: Int
)
