package com.example.backend.controller

import com.example.backend.model.DataToFrontEnd
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVRecord
import org.apache.commons.io.FileUtils
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.io.File
import java.io.FileReader
import java.net.URL
import java.time.LocalDate

@RestController
@CrossOrigin
@RequestMapping("/api/v1/")
class IntensivregisterController {

    @GetMapping("getCSV")
    fun getCSVFile()  : ResponseEntity<DataToFrontEnd> {
        println("lets go")
        val base = URL("https://diviexchange.blob.core.windows.net")
        val full = URL(base, "/%24web/zeitreihe-tagesdaten.csv")
        val dateOfToday = LocalDate.now().toString()
        var frontEndData: DataToFrontEnd? = null
        val file = File("./data.csv")
        FileUtils.copyURLToFile(full, file)
        val readFile = FileReader(file)
        val records: Iterable<CSVRecord> = CSVFormat.Builder.create()
            .setSkipHeaderRecord(true)
            .build()
            .withHeader()
            .parse(readFile)
        for (record in records) {
            if(record[2] == "06531" && record[0] == dateOfToday){
                frontEndData = DataToFrontEnd(
                    covidFalleAktuell = record[5].toInt(),
                    bettenBelegt = record[7].toInt(),
                    bettenFrei = record[8].toInt()
                )
            }
        }
        return ResponseEntity.ok().body(frontEndData)
    }
}