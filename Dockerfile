FROM jjosef/11-jdk-slim-docker
WORKDIR /app
ADD . .
RUN ./gradlew bootJar
EXPOSE 8080
CMD ["java", "-jar", "build/libs/backend-0.0.1-SNAPSHOT.jar"]
